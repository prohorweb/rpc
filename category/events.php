<?php $cat_ID = get_query_var('cat');
if (is_main_site()) {
    $all_title='Все';
} else {
    $all_title='Wszystko';
}
?>
<article>
  <div class="container">
    <h1 class="articles_title f46 mb-3"><?=get_cat_name( $cat_ID ) ?></h1>

    <ul class="nav menu-tabs w-lg-scroll f16 mb-5">
      <li class="nav-item">
        <a href="<?= get_category_link(18);?>"  class="nav-link <?php if (is_category(18)) {echo "active";} ?>"><?= $all_title ?></a>
      </li>
      <li class="nav-item">
        <a href="<?= get_category_link(19);?>"  class="nav-link <?php if (is_category(19)) {echo "active";} ?>"><?=get_cat_name(19) ?></a>
      </li>
      <li class="nav-item">
        <a href="<?= get_category_link(20);?>"  class="nav-link <?php if (is_category(20)) {echo "active";} ?>"><?=get_cat_name(20) ?></a>
      </li>
      <li class="nav-item">
        <a href="<?= get_category_link(21);?>"  class="nav-link <?php if (is_category(21)) {echo "active";} ?>"><?=get_cat_name(21) ?></a>
      </li>
    </ul>


        <div class="row">
          <?php $query = new WP_Query( 'posts_per_page=-1&cat='.$cat_ID);
          if ( $query->have_posts() ) {
                       while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
          <div class="col-lg-3 col-md-4 col-12 mb-3">
            <a href="<?= get_permalink($post_id); ?>">
              <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
            </a>
            <a href="<?= get_permalink($post_id); ?>">
              <div class="f16 text-center my-3 title">
                <?php trim_title_chars(50, '...'); ?>
              </div>
            </a>
          </div>
          <?php endwhile; } else {
        no_found($text);
         }wp_reset_postdata(); ?>
        </div>

    </div>
</article>
<?php get_template_part ('layout/section/partners'); ?>
