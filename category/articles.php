<?php $cat_ID = get_query_var('cat');
if (is_main_site()) {
    $cat_title='КАТЕГОРИИ';
} else {
    $cat_title='KATEGORIE';
} ?>
<article>
  <div class="container">
    <h1 class="articles_title f46"><?=get_cat_name($cat_ID) ?></h1>
    <div class="f12 text-gray-light text-uppercase  mb-4"><?=get_cat_name(9) ?> / <?=get_cat_name($cat_ID) ?></div>
    <div class="row desktop">
      <div class="col-9">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_ID);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                    no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_ID);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12 mt-5">
                <div class="row">
                  <?php       $args = array(
                    'cat' => $cat_ID,
                    'offset' =>3,
                    'posts_per_page' => 999999
                  );
                  $query = new WP_Query($args);
                  while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-4 mb-4">
                    <a href="<?= get_permalink(); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink(); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink(); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile;wp_reset_postdata();?>
                </div>
              </div>
            </div>
      </div>
      <div class="col-3">
        <div class="cat_menu px-3">
          <div class="f16 f-black py-4">
            <b><?= $cat_title ?></b>
          </div>
          <ul class="row p-0 nav nav-tabs">
            <li class="col-6" style="padding-right: 7px;">
              <a href="<?= get_category_link(10);?>" class="cat <?php if (is_category(10)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
                <p class="f12"><?=get_cat_name(10) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a href="<?= get_category_link(11);?>" class="cat <?php if (is_category(11)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
                <p class="f12"><?=get_cat_name(11) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a href="<?= get_category_link(12);?>" class="cat <?php if (is_category(12)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name(12) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a href="<?= get_category_link(13);?>" class="cat <?php if (is_category(13)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name(13) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a href="<?= get_category_link(14);?>" class="cat <?php if (is_category(14)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
                <p class="f12"><?=get_cat_name(14) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a href="<?= get_category_link(15);?>" class="cat <?php if (is_category(15)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
                <p class="f12"><?=get_cat_name(15) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a href="<?= get_category_link(16);?>" class="cat <?php if (is_category(16)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
                <p class="f12"><?=get_cat_name(16) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a href="<?= get_category_link(17);?>" class="cat <?php if (is_category(17)) {
                      echo "active";
                  } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
                <p class="f12"><?=get_cat_name(17) ?></p>
              </a>
            </li>

          </ul>
        </div>
      </div>
    </div>

    <div class="tablet">
      <div class="row">
        <div class="col-12 px-0">
           <div class="cat_menu pt-3">
             <div class="f16 f-black p-3">
               <b><?= $cat_title?></b>
             </div>
             <ul class="p-0 nav nav-tabs w-md-scroll border-0">
               <li class="col cat-mobile">
                 <a href="<?= get_category_link(10);?>" class="  <?php if (is_category(10)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
                   <p class="f12"><?=get_cat_name(10) ?></p>
                 </a>
               </li>
               <li class="col cat-mobile">
                 <a href="<?= get_category_link(11);?>" class="  <?php if (is_category(11)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
                   <p class="f12"><?=get_cat_name(11) ?></p>
                 </a>
               </li>

               <li class="col cat-mobile">
                 <a href="<?= get_category_link(12);?>" class="  <?php if (is_category(12)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                   <p class="f12"><?=get_cat_name(12) ?></p>
                 </a>
               </li>
               <li class="col cat-mobile">
                 <a href="<?= get_category_link(13);?>" class="  <?php if (is_category(13)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                   <p class="f12"><?=get_cat_name(13) ?></p>
                 </a>
               </li>

               <li class="col cat-mobile">
                 <a href="<?= get_category_link(14);?>" class="  <?php if (is_category(14)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
                   <p class="f12"><?=get_cat_name(14) ?></p>
                 </a>
               </li>
               <li class="col cat-mobile">
                 <a href="<?= get_category_link(15);?>" class="  <?php if (is_category(15)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
                   <p class="f12"><?=get_cat_name(15) ?></p>
                 </a>
               </li>

               <li class="col cat-mobile">
                 <a href="<?= get_category_link(16);?>" class="  <?php if (is_category(16)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
                   <p class="f12"><?=get_cat_name(16) ?></p>
                 </a>
               </li>
               <li class="col cat-mobile">
                 <a href="<?= get_category_link(17);?>" class="  <?php if (is_category(17)) {
                      echo "active";
                  } ?>">
                   <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
                   <p class="f12"><?=get_cat_name(17) ?></p>
                 </a>
               </li>
             </ul>
           </div>
        </div>
      </div>

      <div class="row mt-4">
          <?php $query = new WP_Query('posts_per_page=-1&cat='.$cat_ID);
          if ($query->have_posts()) {
              while ($query->have_posts()) : $query->the_post();
              $post_id = get_the_ID(); ?>

          <div class="col-12 col-sm-6">
          <a href="<?= get_permalink($post_id); ?>">
            <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
          </a>
          <div class="date">
            <?= get_the_date('d.m.Y'); ?>
          </div>
          <a href="<?= get_permalink($post_id); ?>">
            <div class="f22 title mb-3">
              <?php trim_title_chars(40, '...'); ?>
            </div>
          </a>
          <a href="<?= get_permalink($post_id); ?>">
            <p class="f16 mb-4">
              <?php trim_excerpt_chars(100, '...'); ?>
            </p>
          </a>
          </div>
        <?php endwhile;
          } else {
              ?>
          <div class="px-4">Материалы отсутствуют. Попробуйте выбрать другую категорию.</div>
          <?php
          }wp_reset_postdata(); ?>
    </div>
  </div>
</article>
<?php get_template_part('layout/section/partners'); ?>
