<?php $cat_ID = get_query_var('cat');
?>
<article>
  <div class="container">
    <h1 class="articles_title f46 mb-3"><?=get_cat_name($cat_ID) ?></h1>
    <ul class="nav menu-tabs w-lg-scroll media">
     <li class="nav-item">
       <a href="<?= get_category_link(23);?>"  class="nav-link <?php if (is_category(23)) {
    echo "active";
} ?>">
         <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-video.svg" alt=""><?=get_cat_name(23) ?></a>
     </li>
     <li class="nav-item">
       <a href="<?= get_category_link(24);?>"  class="nav-link <?php if (is_category(24)) {
    echo "active";
} ?>">
         <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-photo.png" alt=""><?=get_cat_name(24) ?></a>
     </li>
     <li class="nav-item">
       <a href="<?= get_category_link(25);?>"   class="nav-link <?php if (is_category(25)) {
    echo "active";
} ?>">
         <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-audio.svg" alt=""><?=get_cat_name(25) ?></a>
     </li>
     <li class="nav-item">
       <a href="<?= get_category_link(26);?>"  class="nav-link <?php if (is_category(26)) {
    echo "active";
} ?>">
         <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-poligraphia.svg" alt=""><?=get_cat_name(26) ?></a>
     </li>
     <li class="nav-item">
       <a href="<?= get_category_link(27);?>"  class="nav-link <?php if (is_category(27)) {
    echo "active";
} ?>">
         <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-prezentacii.svg" alt=""><?=get_cat_name(27) ?></a>
     </li>
   </ul>



        <div class="row">
          <?php $query = new WP_Query('posts_per_page=-1&cat='.$cat_ID);
          if ($query->have_posts()) {
              while ($query->have_posts()) : $query->the_post();
              $post_id = get_the_ID(); ?>
          <div class="col-lg-3 col-md-4 col-12 mb-3 thumb-media">
            <?php if (has_post_thumbnail($post_id)) {?>
            <a href="<?= get_permalink($post_id); ?>">
              <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
            </a>
           <?php
              } else {
                  the_content();
              } ?>
            <?php
              $pdf = get_field('pdf_file');
              $pptx = get_field('pptx_file');
              if (!empty($pdf)): ?>
            <iframe src="<?php the_field('pdf_file'); ?>?widget=true&amp;headers=true" style="width:100%;height:200px;"></iframe>
             <?php elseif (!empty($pptx)): ?>
             <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=<?php the_field('pptx_file'); ?>' frameborder='0'  style="width:100%;height:200px;"></iframe>
              <?php endif; ?>
            <a href="<?= get_permalink($post_id); ?>">
              <div class="f16 text-center my-3 title">
                <?php trim_title_chars(50, '...'); ?>
              </div>
            </a>
          </div>
          <?php endwhile;
          } else {
              no_found($text);
          }wp_reset_postdata(); ?>
        </div>

    </div>
</article>
<?php get_template_part('layout/section/partners'); ?>
