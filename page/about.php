<?php $post_id = get_the_ID();?>
<div class="container">
  <div class="row">
    <?php $query = new WP_Query( 'p='.$post_id);
                 while($query->have_posts()) : $query->the_post();?>
    <div class="col-12">
      <h1 class="articles_title f46 my-4"><?php the_title(); ?></h1>
      <div class=" col-4 float-right">
          <img class="img-fluid" src="<?= get_the_post_thumbnail_url( $post_id, 'medium' );?>" alt="">
      </div>
      <div class="content">
        <?php the_content(); ?>
      </div>
    </div>

    <?php endwhile; wp_reset_postdata(); ?>

  </div>
</div>
