<?php
 $post_id = get_the_ID();
$category = get_the_category($post_id);
$cat_ID = get_cat_ID($category[0]->cat_name); $post_id= $post->ID; ?>
<article class="container">
  <h1 class="articles_title f36 f-sm-24"><?php the_title(); ?></h1>
  <div class="date m-0 f16">
    <?= get_the_date('d.m.Y');?>
  </div>
  <div class="f12 text-gray-light text-uppercase  my-4"><?=get_cat_name($cat_ID) ?> / <?php the_title(); ?></div>
  <ul class="nav menu-tabs w-lg-scroll media">
   <li class="nav-item">
     <a href="<?= get_category_link(23);?>"  class="nav-link <?php if (is_category(23)) {echo "active";} ?>">
       <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-video.svg" alt=""><?=get_cat_name(23) ?></a>
   </li>
   <li class="nav-item">
     <a href="<?= get_category_link(24);?>"  class="nav-link <?php if (is_category(24)) {echo "active";} ?>">
       <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-photo.png" alt=""><?=get_cat_name(24) ?></a>
   </li>
   <li class="nav-item">
     <a href="<?= get_category_link(25);?>"   class="nav-link <?php if (is_category(25)) {echo "active";} ?>">
       <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-audio.svg" alt=""><?=get_cat_name(25) ?></a>
   </li>
   <li class="nav-item">
     <a href="<?= get_category_link(26);?>"  class="nav-link <?php if (is_category(26)) {echo "active";} ?>">
       <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-poligraphia.svg" alt=""><?=get_cat_name(26) ?></a>
   </li>
   <li class="nav-item">
     <a href="<?= get_category_link(27);?>"  class="nav-link <?php if (is_category(27)) {echo "active";} ?>">
       <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-prezentacii.svg" alt=""><?=get_cat_name(27) ?></a>
   </li>
 </ul>
  <div class="row">
    <div class="col-12">
      <?php $query = new WP_Query('p='.$post_id);
                   while ($query->have_posts()) : $query->the_post();?>
        <div class="content mb-4">
          <?php
            $pdf = get_field('pdf_file');
            $pptx = get_field('pptx_file');
            if (!empty($pdf)): ?>
          <iframe src="<?php the_field('pdf_file'); ?>?widget=true&amp;headers=true" style="width:100%;height:100vh;"></iframe>
           <? elseif (!empty($pptx)): ?>
           <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=<?php the_field('pptx_file'); ?>' frameborder='0'  style="width:100%;height:100vh;"></iframe>
            <?php endif; ?>
          <?php the_content(); ?>
        </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>
  </div>


  <div class="row">
      <?php
      $args = array(
        'cat' => $cat_ID,
        'post__not_in'  => array( $post_id ),
        'posts_per_page' => 3
      );
      $query = new WP_Query($args);
          while ($query->have_posts()) : $query->the_post(); ?>
      <div class="col-md-4 col-12">
        <a href="<?php the_permalink(); ?>">
          <img class="w-100" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="">
        </a>
        <div class="date f12">
          <?= get_the_date('d.m.Y'); ?>
        </div>
        <a href="<?php the_permalink(); ?>">
          <div class="title mb-3">
            <b><?php trim_title_chars(40, '...'); ?></b>
          </div>
        </a>
        <a href="<?php the_permalink(); ?>">
          <p class=" f16 mb-4">
            <?php trim_excerpt_chars(100, '...'); ?>
          </p>
        </a>
      </div>
    <?php endwhile; wp_reset_postdata();
    $cat_count=get_category($cat_ID)->category_count;
if ($cat_count > 3){?>
  <div class="d-flex justify-content-center w-100 mt-3">
      <a href="<?= get_category_link($cat_ID); ?>" class="button f14 d-flex align-items-center">
        <p><?php  btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
      </a>
  </div>
</div>
<?php } ?>


</div>
</article>
