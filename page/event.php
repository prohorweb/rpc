<?php $post_id = get_the_ID();
$category = get_the_category($post_id);
$cat_ID = get_cat_ID($category[0]->cat_name);

if (is_main_site()) {
    $more_title='Еще мероприятия';
} else {
    $more_title='Więcej wydarzeń';
}
?>
<article class="container">
  <h1 class="articles_title f36 f-sm-24">
    <?php the_title(); ?>
  </h1>
  <div class="f12 text-gray-light text-uppercase  my-4">
    <?=get_cat_name($cat_ID) ?> /
    <?php the_title(); ?>
  </div>
  <div class="row">
    <div class="col-12 col-md-9">
      <?php $query = new WP_Query('p='.$post_id);
                   while ($query->have_posts()) : $query->the_post();?>
      <div class="row mb-4">
        <div class="col-md-8 col-12">
          <img class="w-100" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="<?php the_title(); ?>">
        </div>
        <div class="col-md-4 col-12">
          <b>
            <?php the_excerpt(); ?></b>
          <div class="date f16 d-flex align-items-center">
            <i class="far fa-clock f28 f-blue col-2 p-0"></i>
            <div class="col p-0">
            <?= get_the_date('d.m.Y'); ?>
            </div>
          </div>
          <?php
            $location = get_field('location');
            if (!empty($location)): ?>
            <div class="date f16 d-flex">
              <i class="fas fa-map-marker-alt f36 f-blue col-2 p-0"></i>
              <div class="colp-0"><?php the_field('location'); ?></div>
            </div>
            <?php endif; ?>

        </div>
      </div>
      <div class="content mb-4">
        <?php the_content();?>
      </div>
      <div class="row">
        <div class="col-5 pr-0">
          <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
          <script src="//yastatic.net/share2/share.js"></script>
          <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter,linkedin,lj,viber,whatsapp,skype,telegram" data-counter=""></div>
        </div>
        <div class="col-7">
          <?php
            $categories = get_the_category();
            foreach ($categories as $category) {
                echo '<a class="mr-1" href="' . get_category_link($category->term_id) . '" title="' . sprintf(__("View all posts in %s"), $category->name) . '" ' . '>#' . $category->name . '</a>  ';
            }
            ?>
        </div>
      </div>
      <div class="comments">
        <?php comments_template('/layout/section/comments.php'); ?>
      </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div class="col-12 col-md-3">
      <p><b class="f20"><?= $more_title; ?>:</b></p>
      <?php
          $args = array(
            'cat' => $cat_ID,
            'post__not_in'  => array( $post_id ),
            'posts_per_page' => 3
          );
          $query = new WP_Query($args);
              while ($query->have_posts()) : $query->the_post(); ?>

      <div class="w-100">
        <a href="<?php the_permalink(); ?>">
          <img class="w-100" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="">
        </a>
        <div class="date f12">
          <?= get_the_date('d.m.Y'); ?>
        </div>
        <a href="<?php the_permalink(); ?>">
          <div class="title mb-3">
            <b>
              <?php trim_title_chars(40, '...'); ?></b>
          </div>
        </a>
        <a href="<?php the_permalink(); ?>">
          <p class="f16 mb-4">
            <?php trim_excerpt_chars(100, '...'); ?>
          </p>
        </a>
      </div>
      <?php endwhile;wp_reset_postdata();   $cat_count=get_category($cat_ID)->category_count;
      if ($cat_count > 3) {
          ?>
      <div class="d-flex justify-content-center w-100 mt-3">
        <a href="<?= get_category_link($cat_ID); ?>" class="button f14 d-flex align-items-center">
          <p><?php  btn_event($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
        </a>
      </div>
      <?php
      } ?>
    </div>
  </div>
</article>
