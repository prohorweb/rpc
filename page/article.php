<?php $post_id = get_the_ID();
$category = get_the_category($post_id);
$cat_ID = get_cat_ID($category[0]->cat_name);

if (is_main_site()) {
    $cat_title='КАТЕГОРИИ';
} else {
    $cat_title='KATEGORIE';
}
?>
<article class="container">
  <h1 class="articles_title f36 f-sm-24"><?php the_title(); ?></h1>
  <div class="date m-0 f16">
    <?= get_the_date('d.m.Y');?>
  </div>
  <div class="f12 text-gray-light text-uppercase  my-4"><?=get_cat_name($cat_ID) ?> / <?php the_title(); ?></div>
  <div class="row">
    <div class="col-12 col-lg-9">
      <?php $query = new WP_Query('p='.$post_id);
                   while ($query->have_posts()) : $query->the_post();?>
        <div class="content mb-4">
          <?php the_content(); ?>
        </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div class="col-12 col-lg-3 d-none d-lg-block">
      <div class="cat_menu px-3 ">
        <div class="f16 f-black py-4">
          <b><?= $cat_title ?></b>
        </div>
        <ul class="row p-0 nav nav-tabs">
          <li class="col-6" style="padding-right: 7px;">
            <a href="<?= get_category_link(10);?>" class="cat <?php if (is_category(10)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
              <p class="f12"><?=get_cat_name(10) ?></p>
            </a>
          </li>
          <li class="col-6" style="padding-left: 7px;">
            <a href="<?= get_category_link(11);?>" class="cat <?php if (is_category(11)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
              <p class="f12"><?=get_cat_name(11) ?></p>
            </a>
          </li>

          <li class="col-6" style="padding-right: 7px;">
            <a href="<?= get_category_link(12);?>" class="cat <?php if (is_category(12)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
              <p class="f12"><?=get_cat_name(12) ?></p>
            </a>
          </li>
          <li class="col-6" style="padding-left: 7px;">
            <a href="<?= get_category_link(13);?>" class="cat <?php if (is_category(13)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
              <p class="f12"><?=get_cat_name(13) ?></p>
            </a>
          </li>

          <li class="col-6" style="padding-right: 7px;">
            <a href="<?= get_category_link(14);?>" class="cat <?php if (is_category(14)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
              <p class="f12"><?=get_cat_name(14) ?></p>
            </a>
          </li>
          <li class="col-6" style="padding-left: 7px;">
            <a href="<?= get_category_link(15);?>" class="cat <?php if (is_category(15)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
              <p class="f12"><?=get_cat_name(15) ?></p>
            </a>
          </li>

          <li class="col-6" style="padding-right: 7px;">
            <a href="<?= get_category_link(16);?>" class="cat <?php if (is_category(16)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
              <p class="f12"><?=get_cat_name(16) ?></p>
            </a>
          </li>
          <li class="col-6" style="padding-left: 7px;">
            <a href="<?= get_category_link(17);?>" class="cat <?php if (is_category(17)) {
                    echo "active";
                } ?>">
              <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
              <p class="f12"><?=get_cat_name(17) ?></p>
            </a>
          </li>

        </ul>
      </div>
    </div>
  </div>
  <div class="tablet">
    <div class="row">
      <div class="col-12 px-0">
        <div class="cat_menu pt-3">
          <div class="f16 f-black p-3">
            <b><?= $cat_title?></b>
          </div>
          <ul class="p-0 nav nav-tabs w-md-scroll border-0">
            <li class="col cat-mobile">
              <a href="<?= get_category_link(10);?>" class="  <?php if (is_category(10)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
                <p class="f12"><?=get_cat_name(10) ?></p>
              </a>
            </li>
            <li class="col cat-mobile">
              <a href="<?= get_category_link(11);?>" class="  <?php if (is_category(11)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
                <p class="f12"><?=get_cat_name(11) ?></p>
              </a>
            </li>

            <li class="col cat-mobile">
              <a href="<?= get_category_link(12);?>" class="  <?php if (is_category(12)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name(12) ?></p>
              </a>
            </li>
            <li class="col cat-mobile">
              <a href="<?= get_category_link(13);?>" class="  <?php if (is_category(13)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name(13) ?></p>
              </a>
            </li>

            <li class="col cat-mobile">
              <a href="<?= get_category_link(14);?>" class="  <?php if (is_category(14)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
                <p class="f12"><?=get_cat_name(14) ?></p>
              </a>
            </li>
            <li class="col cat-mobile">
              <a href="<?= get_category_link(15);?>" class="  <?php if (is_category(15)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
                <p class="f12"><?=get_cat_name(15) ?></p>
              </a>
            </li>

            <li class="col cat-mobile">
              <a href="<?= get_category_link(16);?>" class="  <?php if (is_category(16)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
                <p class="f12"><?=get_cat_name(16) ?></p>
              </a>
            </li>
            <li class="col cat-mobile">
              <a href="<?= get_category_link(17);?>" class="  <?php if (is_category(17)) {
                   echo "active";
               } ?>">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
                <p class="f12"><?=get_cat_name(17) ?></p>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
      <?php
      $args = array(
        'cat' => $cat_ID,
        'post__not_in'  => array( $post_id ),
        'posts_per_page' => 3
      );
      $query = new WP_Query($args);
          while ($query->have_posts()) : $query->the_post(); ?>
      <div class="col-md-4 col-12">
        <a href="<?php the_permalink(); ?>">
          <img class="w-100" src="<?= the_post_thumbnail_url('thumbnail'); ?>" alt="">
        </a>
        <div class="date f12">
          <?= get_the_date('d.m.Y'); ?>
        </div>
        <a href="<?php the_permalink(); ?>">
          <div class="title mb-3">
            <b><?php trim_title_chars(40, '...'); ?></b>
          </div>
        </a>
        <a href="<?php the_permalink(); ?>">
          <p class=" f16 mb-4">
            <?php trim_excerpt_chars(100, '...'); ?>
          </p>
        </a>
      </div>
    <?php endwhile; wp_reset_postdata();
    $cat_count=get_category($cat_ID)->category_count;
if ($cat_count > 3){?>
  <div class="d-flex justify-content-center w-100 mt-3">
      <a href="<?= get_category_link($cat_ID); ?>" class="button f14 d-flex align-items-center">
        <p><?php btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
      </a>
  </div>
</div>
<?php } ?>


</div>
</article>
