
 <section id="partners-link" class="partners">
  <div class="container">
     <h1 class="articles_title f46  text-center"><?=get_cat_name(28) ?></h1>
    <div class="slider-nav">
      <?php  $query = new WP_Query('cat=28&posts_per_page=-1');
                    while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID(); ?>
    	<div class="item">
    		 <img class="img-fluid" src="<?php the_post_thumbnail_url( 'thumbnail' );?>" alt=""  draggable="false"/>
    	</div>
    <?php endwhile; wp_reset_postdata(); ?>
    </div>


      <div class="slider-for">
        <?php  $query = new WP_Query('cat=28&posts_per_page=-1');
                      while ($query->have_posts()) : $query->the_post(); ?>
      	<div class="item">
          <div class="justify-content-center d-flex">
            <p class="col-md-9 col-12 text-center">
              <a href="<?php echo strip_tags(get_the_content());?>" target='_blank'><?php the_title(); ?></a>
            </p>
          </div>
       </div>
         <?php endwhile; wp_reset_postdata(); ?>

  </div>
</section>
