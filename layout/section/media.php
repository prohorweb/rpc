<?php
if (is_main_site()) {
    $last_title='Последние публикации:';
} else {
    $last_title='Najnowsze publikacje:';
}
 ?>
 <ul class="nav menu-tabs w-lg-scroll" id="mediaTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="mediaone-tab" data-toggle="tab" href="#mediaone" role="tab" aria-controls="mediaone" aria-selected="true">
      <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-video.svg" alt=""><?=get_cat_name(23) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="mediatwo-tab" data-toggle="tab" href="#mediatwo" role="tab" aria-controls="mediatwo" aria-selected="false">
      <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-photo.png" alt=""><?=get_cat_name(24) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="mediathree-tab" data-toggle="tab" href="#mediathree" role="tab" aria-controls="mediathree" aria-selected="false">
      <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-audio.svg" alt=""><?=get_cat_name(25) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="mediafour-tab" data-toggle="tab" href="#mediafour" role="tab" aria-controls="mediafour" aria-selected="false">
      <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-poligraphia.svg" alt=""><?=get_cat_name(26) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="mediafive-tab" data-toggle="tab" href="#mediafive" role="tab" aria-controls="mediafive" aria-selected="false">
      <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-prezentacii.svg" alt=""><?=get_cat_name(27) ?></a>
  </li>
</ul>
<div class="tab-content" id="mediaTabContent">
  <div class="tab-pane fade show active" id="mediaone" role="tabpanel" aria-labelledby="mediaone-tab">
    <div class="row">
      <div class="col-md-6 col-12 tab-thumb-media">
        <?php $query = new WP_Query('posts_per_page=1&cat=23');
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();?>
        <?php the_content() ?>
        <div class="text-media mt-3 pl-0">
        <a href="<?php the_permalink() ?>">
          <b><?= get_the_date('d.m.Y'); ?> <?php the_title() ?></b>
          <p><?php echo strip_tags(get_the_excerpt());?></p>
        </a>
        </div>
        <?php endwhile;
        } else {
          no_found($text);
        }wp_reset_postdata(); ?>

      </div>
      <div class="col-md-6 col-12">
        <div class="last-media f28"><?= $last_title ?></div>
        <div class="row">
          <?php $query = new WP_Query('posts_per_page=4&offset=1&cat=23');
              while ($query->have_posts()) : $query->the_post();?>
          <div class="col-md-6 col-12 d-flex align-items-start">
            <div class="col-2 p-0">
              <img class="w-100" src="<?php bloginfo('template_url');?>/assets/img/icon-video.svg" alt="">
            </div>
            <div class="col-10 p-0">
              <div class="text-media">
               <a href="<?php the_permalink() ?>">
                <b><?= get_the_date('d.m.Y'); ?></b>
                <p><?php the_title() ?></p>
               </a>
              </div>
            </div>
          </div>
        <?php endwhile;wp_reset_postdata();?>
          <div class="d-flex justify-content-center w-100 mt-4">
           <a href="<?=  get_category_link(23); ?>" class="button f14 d-flex align-items-center "><p> <?php btn_all($text)?></p> <i class="fas fa-chevron-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="mediatwo" role="tabpanel" aria-labelledby="mediatwo-tab">
    <div class="row">
      <div class="col-md-6 col-12">
        <?php $query = new WP_Query('posts_per_page=1&cat=24');
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();?>
          <a href="<?php the_permalink() ?>">
            <img class="w-100" src="<?=the_post_thumbnail_url('thumbnail'); ?>" alt="">
          </a>
        <div class="text-media mt-3 pl-0">
        <a href="<?php the_permalink() ?>">
          <b><?= get_the_date('d.m.Y'); ?> <?php the_title() ?></b>
          <p><?php echo strip_tags(get_the_excerpt());?></p>
        </a>
        </div>
        <?php endwhile;
        } else {
          no_found($text);
        }wp_reset_postdata(); ?>

      </div>
      <div class="col-md-6 col-12">
        <div class="last-media f28"><?= $last_title ?></div>
        <div class="row">
          <?php $query = new WP_Query('posts_per_page=4&offset=1&cat=24');
              while ($query->have_posts()) : $query->the_post();?>
          <div class="col-md-6 col-12 d-flex align-items-start">
            <div class="col-2 p-0">
              <img class="w-100" src="<?php bloginfo('template_url');?>/assets/img/icon-photo.png" alt="">
            </div>
            <div class="col-10 p-0">
              <div class="text-media">
               <a href="<?php the_permalink() ?>">
                <b><?= get_the_date('d.m.Y'); ?></b>
                <p><?php the_title() ?></p>
               </a>
              </div>
            </div>
          </div>
        <?php endwhile;wp_reset_postdata();?>
          <div class="d-flex justify-content-center w-100 mt-4">
           <a href="<?=  get_category_link(24); ?>" class="button f14 d-flex align-items-center "><p> <?php btn_all($text)?></p> <i class="fas fa-chevron-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane fade" id="mediathree" role="tabpanel" aria-labelledby="mediathree-tab">
    <div class="row">
      <div class="col-md-6 col-12">
        <?php $query = new WP_Query('posts_per_page=1&cat=25');
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();?>
            <img class="w-100" src="<?=the_post_thumbnail_url('thumbnail'); ?>" alt="">
        <?php the_content() ?>
        <div class="text-media mt-3 pl-0">
        <a href="<?php the_permalink() ?>">
          <b><?= get_the_date('d.m.Y'); ?> <?php the_title() ?></b>
          <p><?php echo strip_tags(get_the_excerpt());?></p>
        </a>
        </div>
        <?php endwhile;
        } else {
          no_found($text);
        }wp_reset_postdata(); ?>

      </div>
      <div class="col-md-6 col-12">
        <div class="last-media f28"><?= $last_title ?></div>
        <div class="row">
          <?php $query = new WP_Query('posts_per_page=4&offset=1&cat=25');
              while ($query->have_posts()) : $query->the_post();?>
          <div class="col-md-6 col-12 d-flex align-items-start">
            <div class="col-2 p-0">
              <img class="w-100" src="<?php bloginfo('template_url');?>/assets/img/icon-audio.svg" alt="">
            </div>
            <div class="col-10 p-0">
              <div class="text-media">
               <a href="<?php the_permalink() ?>">
                <b><?= get_the_date('d.m.Y'); ?></b>
                <p><?php the_title() ?></p>
               </a>
              </div>
            </div>
          </div>
        <?php endwhile;wp_reset_postdata();?>
          <div class="d-flex justify-content-center w-100 mt-4">
           <a href="<?=  get_category_link(25); ?>" class="button f14 d-flex align-items-center "><p> <?php btn_all($text)?></p> <i class="fas fa-chevron-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane fade pb-5" id="mediafour" role="tabpanel" aria-labelledby="mediafour-tab">
    <div class="row">
      <div class="col-md-6 col-12">
        <?php $query = new WP_Query('posts_per_page=1&cat=26');
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();?>
            <?php
              $pdf = get_field('pdf_file');
              if (!empty($pdf)): ?>
            <iframe src="<?php the_field('pdf_file'); ?>?widget=true&amp;headers=true" style="width:100%;height:100%;"></iframe>
             <? elseif (empty($pdf)): ?>
             <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=<?php the_field('pptx_file'); ?>' width='100%' height='100%' frameborder='0'></iframe>
              <?php endif; ?>
        <div class="text-media my-3 pl-0">
        <a href="<?php the_permalink() ?>">
          <b><?= get_the_date('d.m.Y'); ?> <?php the_title() ?></b>
          <p>
            <?php echo strip_tags(get_the_excerpt());?>
          </p>
        </a>
        </div>
        <?php endwhile;
        } else {
          no_found($text);
        }wp_reset_postdata(); ?>

      </div>
      <div class="col-md-6 col-12">
        <div class="last-media f28"><?= $last_title ?></div>
        <div class="row">
          <?php $query = new WP_Query('posts_per_page=4&offset=1&cat=26');
              while ($query->have_posts()) : $query->the_post();?>
          <div class="col-md-6 col-12 d-flex align-items-start">
            <div class="col-2 p-0">
              <img class="w-100" src="<?php bloginfo('template_url');?>/assets/img/icon-poligraphia.svg" alt="">
            </div>
            <div class="col-10 p-0">
              <div class="text-media">
               <a href="<?php the_permalink() ?>">
                <b><?= get_the_date('d.m.Y'); ?></b>
                <p><?php the_title() ?></p>
               </a>
              </div>
            </div>
          </div>
        <?php endwhile;wp_reset_postdata();?>
          <div class="d-flex justify-content-center w-100 mt-4">
           <a href="<?=  get_category_link(26); ?>" class="button f14 d-flex align-items-center "><p> <?php btn_all($text)?></p> <i class="fas fa-chevron-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tab-pane fade pb-5" id="mediafive" role="tabpanel" aria-labelledby="mediafive-tab">
    <div class="row">
      <div class="col-md-6 col-12">
        <?php $query = new WP_Query('posts_per_page=1&cat=27');
        if ($query->have_posts()) {
            while ($query->have_posts()) : $query->the_post();?>
            <?php
              $pdf = get_field('pdf_file');
              $pptx = get_field('pptx_file');
              if (!empty($pdf)): ?>
            <iframe src="<?php the_field('pdf_file'); ?>?widget=true&amp;headers=true" style="width:100%;height:100%;"></iframe>
             <? elseif (!empty($pptx)): ?>
             <iframe src='https://view.officeapps.live.com/op/embed.aspx?src=<?php the_field('pptx_file'); ?>' frameborder='0'  style="width:100%;height:100%;"></iframe>
              <?php endif; ?>
        <div class="text-media my-3 pl-0">
        <a href="<?php the_permalink() ?>">
          <b><?= get_the_date('d.m.Y'); ?> <?php the_title() ?></b>
          <p><?php echo strip_tags(get_the_excerpt());?></p>
        </a>
        </div>
        <?php endwhile;
        } else {
          no_found($text);
        }wp_reset_postdata(); ?>

      </div>
      <div class="col-md-6 col-12">
        <div class="last-media f28"><?= $last_title ?></div>
        <div class="row">
          <?php $query = new WP_Query('posts_per_page=4&offset=1&cat=27');
              while ($query->have_posts()) : $query->the_post();?>
          <div class="col-md-6 col-12 d-flex align-items-start">
            <div class="col-2 p-0">
              <img class="w-100" src="<?php bloginfo('template_url');?>/assets/img/icon-prezentacii.svg" alt="">
            </div>
            <div class="col-10 p-0">
              <div class="text-media">
               <a href="<?php the_permalink() ?>">
                <b><?= get_the_date('d.m.Y'); ?></b>
                <p><?php the_title() ?></p>
               </a>
              </div>
            </div>
          </div>
        <?php endwhile;wp_reset_postdata();?>
          <div class="d-flex justify-content-center w-100 mt-4">
           <a href="<?=  get_category_link(27); ?>" class="button f14 d-flex align-items-center "><p> <?php btn_all($text)?></p> <i class="fas fa-chevron-circle-right"></i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
