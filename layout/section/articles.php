<?php $cat_actualno=10;
      $cat_anonce=11;
      $cat_smi_pol=13;
      $cat_smi_rus=12;
      $cat_public=14;
      $cat_inter=15;
      $cat_nauka=16;
      $cat_media=17;

      $cat_link_actualno=get_category_link(10);
      $cat_link_anonce=get_category_link(11);
      $cat_link_smi_rus=get_category_link(12);
      $cat_link_smi_pol=get_category_link(13);
      $cat_link_public=get_category_link(14);
      $cat_link_inter=get_category_link(15);
      $cat_link_nauka=get_category_link(16);
      $cat_link_media=get_category_link(17);

      if (is_main_site()) {
          $cat_title='КАТЕГОРИИ';
      } else {
          $cat_title='KATEGORIE';
      }
      ?>

    <div class="row desktop">
      <div class="col-9">
        <div class="tab-content" id="articlesTabContent">
          <div class="tab-pane fade show active" id="art-actualno" role="tabpanel" aria-labelledby="art-actualno-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_actualno);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_actualno);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(80, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_actualno);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_actualno)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_inter; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-anonce" role="tabpanel" aria-labelledby="art-anonce-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_anonce);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_anonce);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_anonce);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_anonce)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_anonce; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-smi-russia" role="tabpanel" aria-labelledby="art-smi-russia-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_smi_rus);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_smi_rus);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_smi_rus);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_smi_rus)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_smi_rus; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-smi-polland" role="tabpanel" aria-labelledby="art-smi-polland-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_smi_pol);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_smi_pol);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_smi_pol);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_smi_pol)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_smi_pol; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-public" role="tabpanel" aria-labelledby="art-public-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_public);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_public);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_public);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_public)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_public; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-intervu" role="tabpanel" aria-labelledby="art-intervu-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_inter);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_inter);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_inter);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                  <?php
                            } wp_reset_postdata();
                        $cat_count=get_category($cat_inter)->category_count;
                    if ($cat_count > 12) {
                        ?>
                      <div class=" d-flex justify-content-center w-100">
                        <div class="all-art-btn">
                          <a href="<?= $cat_link_inter; ?>" class="button f14 d-flex align-items-center">
                            <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                          </a>
                        </div>
                      </div>
                 <?php
                    } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-nauka" role="tabpanel" aria-labelledby="art-nauka-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_nauka);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_nauka);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_nauka);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_nauka)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_nauka; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art-media" role="tabpanel" aria-labelledby="art-media-tab">
            <div class="row">
              <div class="col-8">
                <?php $query = new WP_Query('posts_per_page=1&cat='.$cat_media);
                if ($query->have_posts()) {
                    while ($query->have_posts()) : $query->the_post();
                    $post_id = get_the_ID(); ?>
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'large'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="f22 title">
                    <?php trim_title_chars(105, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(300, '...'); ?>
                  </p>
                </a>
                <a href="<?= get_permalink($post_id); ?>" class="read-more f16"><?php btn_more($text) ?> <i class="fas fa-arrow-right"></i></a>
                <?php endwhile;
                } else {
                  no_found($text);
                }wp_reset_postdata(); ?>
              </div>
              <div class="col-4">
                <div class="row">
                  <?php $query = new WP_Query('posts_per_page=2&offset=1&cat='.$cat_media);
                               while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col-12 mb-4">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(60, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>

                  <?php endwhile; wp_reset_postdata(); ?>
                </div>
              </div>
              <div class="col-12">
                <div class="row">
                  <?php  $query = new WP_Query('posts_per_page=9&offset=3&cat='.$cat_media);
                            if ($query->have_posts()) {
                                while ($query->have_posts()) : $query->the_post();
                                $post_id = get_the_ID(); ?>
                  <div class="col-4 mb-4  articles-all">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; ?>
                  <div class="d-flex justify-content-center w-100 ">
                    <div class="more-btn button f14 d-flex align-items-center">
                      <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                    </div>
                  </div>
                <?php
                            } wp_reset_postdata();
                      $cat_count=get_category($cat_media)->category_count;
                  if ($cat_count > 12) {
                      ?>
                    <div class=" d-flex justify-content-center w-100">
                      <div class="all-art-btn">
                        <a href="<?= $cat_link_media; ?>" class="button f14 d-flex align-items-center">
                          <p><?php btn_all($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
                        </a>
                      </div>
                    </div>
               <?php
                  } ?>
                </div>
              </div>
            </div>
          </div> <!--  tab-pane-->
        </div> <!--  tab-content -->
      </div>
      <div class="col-3">
        <div class="cat_menu px-3">
          <div class="f16 f-black py-4">
            <b><?= $cat_title; ?></b>
          </div>
          <ul class="row p-0 nav nav-tabs" id="articlesTab" role="tablist">
            <li class="col-6" style="padding-right: 7px;">
              <a class="cat active" id="art-actualno-tab" data-toggle="tab" href="#art-actualno" role="tab" aria-controls="art-actualno" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
                <p class="f12"><?=get_cat_name($cat_actualno) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a class="cat" id="art-anonce-tab" data-toggle="tab" href="#art-anonce" role="tab" aria-controls="art-anonce" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
                <p class="f12"><?=get_cat_name($cat_anonce) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a class="cat" id="art-smi-russia-tab" data-toggle="tab" href="#art-smi-russia" role="tab" aria-controls="art-smi-russia" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name($cat_smi_rus) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a class="cat" id="art-smi-polland-tab" data-toggle="tab" href="#art-smi-polland" role="tab" aria-controls="art-smi-polland" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name($cat_smi_pol) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a class="cat" id="art-public-tab" data-toggle="tab" href="#art-public" role="tab" aria-controls="art-public" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
                <p class="f12"><?=get_cat_name($cat_public) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a class="cat" id="art-intervu-tab" data-toggle="tab" href="#art-intervu" role="tab" aria-controls="art-intervu" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
                <p class="f12"><?=get_cat_name($cat_inter) ?></p>
              </a>
            </li>

            <li class="col-6" style="padding-right: 7px;">
              <a class="cat" id="art-nauka-tab" data-toggle="tab" href="#art-nauka" role="tab" aria-controls="art-nauka" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
                <p class="f12"><?=get_cat_name($cat_nauka) ?></p>
              </a>
            </li>
            <li class="col-6" style="padding-left: 7px;">
              <a class="cat" id="art-media-tab" data-toggle="tab" href="#art-media" role="tab" aria-controls="art-media" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
                <p class="f12"><?=get_cat_name($cat_media) ?></p>
              </a>
            </li>

          </ul>
        </div>
      </div>
    </div>

    <div class="row tablet">
      <div class="col-12 px-0">
         <div class="cat_menu pt-3">
           <ul class="p-0 nav nav-tabs w-md-scroll border-0" id="articles_mobileTab" role="tablist">
            <li class="cat-mobile col active">
              <a id="art_mobile-actualno-tab" data-toggle="tab" href="#art_mobile-actualno" role="tab" aria-controls="art_mobile-actualno" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-actualno.png" alt="">
                <p class="f12"><?=get_cat_name($cat_actualno) ?></p>
              </a>
            </li>
            <li class="cat-mobile col">
              <a id="art_mobile-anonce-tab" data-toggle="tab" href="#art_mobile-anonce" role="tab" aria-controls="art_mobile-anonce" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-anonce.png" alt="">
                <p class="f12"><?=get_cat_name($cat_anonce) ?></p>
              </a>
            </li>
            <li class="cat-mobile col">
              <a id="art_mobile-smi-russia-tab" data-toggle="tab" href="#art_mobile-smi-russia" role="tab" aria-controls="art_mobile-smi-russia" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name($cat_smi_rus) ?></p>
              </a>
            </li>
            <li class="cat-mobile col">
              <a id="art_mobile-smi-polland-tab" data-toggle="tab" href="#art_mobile-smi-polland" role="tab" aria-controls="art_mobile-smi-polland" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-smi.svg" alt="">
                <p class="f12"><?=get_cat_name($cat_smi_pol) ?></p>
              </a>
            </li>

            <li class="cat-mobile col">
              <a id="art_mobile-public-tab" data-toggle="tab" href="#art_mobile-public" role="tab" aria-controls="art_mobile-public" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-public.png" alt="">
                <p class="f12"><?=get_cat_name($cat_public) ?></p>
              </a>
            </li>
            <li class="cat-mobile col">
              <a id="art_mobile-intervu-tab" data-toggle="tab" href="#art_mobile-intervu" role="tab" aria-controls="art_mobile-intervu" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-intervu.png" alt="">
                <p class="f12"><?=get_cat_name($cat_inter) ?></p>
              </a>
            </li>

            <li class="cat-mobile col">
              <a id="art_mobile-nauka-tab" data-toggle="tab" href="#art_mobile-nauka" role="tab" aria-controls="art_mobile-nauka" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-nauka.png" alt="">
                <p class="f12"><?=get_cat_name($cat_nauka) ?></p>
              </a>
            </li>
            <li class="cat-mobile col">
              <a id="art_mobile-media-tab" data-toggle="tab" href="#art_mobile-media" role="tab" aria-controls="art_mobile-media" aria-selected="true">
                <img class="img-fluid" src="<?php bloginfo('template_url');?>/assets/img/icon-media.png" alt="">
                <p class="f12"><?=get_cat_name($cat_media) ?></p>
              </a>
            </li>
           </ul>
         </div>
      </div>

      <div class="col-12">
        <div class="tab-content" id="articles_mobileTabContent">
          <div class="tab-pane fade show active" id="art_mobile-actualno" role="tabpanel" aria-labelledby="art_mobile-actualno-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_actualno);
                                while ($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
                  <div class="col">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y');?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                  <?php endwhile; wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-anonce" role="tabpanel" aria-labelledby="art_mobile-anonce-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_anonce);
                      if ($query->have_posts()) {
                          while ($query->have_posts()) : $query->the_post();
                          $post_id = get_the_ID(); ?>
                  <div class="col">
                    <a href="<?= get_permalink($post_id); ?>">
                      <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                    </a>
                    <div class="date">
                      <?= get_the_date('d.m.Y'); ?>
                    </div>
                    <a href="<?= get_permalink($post_id); ?>">
                      <div class="title">
                        <?php trim_title_chars(50, '...'); ?>
                      </div>
                    </a>
                    <a href="<?= get_permalink($post_id); ?>">
                      <p class="f16 my-2">
                        <?php trim_excerpt_chars(100, '...'); ?>
                      </p>
                    </a>
                  </div>
                <?php endwhile;
                      } else {
                        no_found($text);
                      }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-smi-russia" role="tabpanel" aria-labelledby="art_mobile-smi-russia-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_smi_rus);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-smi-polland" role="tabpanel" aria-labelledby="art_mobile-smi-polland-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_smi_pol);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-public" role="tabpanel" aria-labelledby="art_mobile-public-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_public);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-intervu" role="tabpanel" aria-labelledby="art_mobile-intervu-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_inter);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-nauka" role="tabpanel" aria-labelledby="art_mobile-nauka-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_nauka);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
          <div class="tab-pane fade" id="art_mobile-media" role="tabpanel" aria-labelledby="art_mobile-media-tab">
            <div class="col-mobile_articles w-lg-scroll">
                  <?php  $query = new WP_Query('posts_per_page=-1&cat='.$cat_media);
                  if ($query->have_posts()) {
                      while ($query->have_posts()) : $query->the_post();
                      $post_id = get_the_ID(); ?>
              <div class="col">
                <a href="<?= get_permalink($post_id); ?>">
                  <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail'); ?>" alt="">
                </a>
                <div class="date">
                  <?= get_the_date('d.m.Y'); ?>
                </div>
                <a href="<?= get_permalink($post_id); ?>">
                  <div class="title">
                    <?php trim_title_chars(50, '...'); ?>
                  </div>
                </a>
                <a href="<?= get_permalink($post_id); ?>">
                  <p class="f16 my-2">
                    <?php trim_excerpt_chars(100, '...'); ?>
                  </p>
                </a>
              </div>
            <?php endwhile;
                  } else {
                    no_found($text);
                  }wp_reset_postdata(); ?>
            </div>
          </div> <!--  tab-pane-->
        </div> <!--  tab-content -->
      </div>

    </div>
