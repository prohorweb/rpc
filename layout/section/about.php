<?php
if (is_main_site()) {
    $more_btn='Узнать больше';
} else {
    $more_btn = 'Dowiedz się więcej';
}
 ?><div class="container">
  <div class="text-center">
    <img class="flag" src="<?php bloginfo('template_url');?>/assets/img/flag.png" alt="">
    <h1 class="about_title f46 "><?=get_cat_name(4) ?></h1>
  </div>
  <ul class="nav menu-tabs w-lg-scroll f16" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="pills-about-tab" data-toggle="pill" href="#pills-about" role="tab" aria-controls="pills-about" aria-selected="true"><?=get_cat_name(4) ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-director-tab" data-toggle="pill" href="#pills-director" role="tab" aria-controls="pills-director" aria-selected="false"><?= $excerpt = get_the_excerpt( 699 );?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-board-tab" data-toggle="pill" href="#pills-board" role="tab" aria-controls="pills-board" aria-selected="false"><?=get_cat_name(5) ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-care-tab" data-toggle="pill" href="#pills-care" role="tab" aria-controls="pills-care" aria-selected="false"><?=get_cat_name(6) ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-science-tab" data-toggle="pill" href="#pills-science" role="tab" aria-controls="pills-science" aria-selected="false"><?=get_cat_name(7) ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="pills-acts-tab" data-toggle="pill" href="#pills-acts" role="tab" aria-controls="pills-acts" aria-selected="false"><?=get_cat_name(8) ?></a>
    </li>
  </ul>
  <div class="tab-content" id="pills-tabContent">
    <div class="tab-pane fade show active py-5 " id="pills-about" role="tabpanel" aria-labelledby="pills-about-tab">
      <?php $query = new WP_Query( 'p=152');
                       while($query->have_posts()) : $query->the_post();?>
      <div class="row">
        <div class="col-12">
          <?php the_excerpt(); ?>
        </div>
      </div>
      <div class="d-flex justify-content-center mt-5">
        <a href="<?= get_permalink(152); ?>" class="button f14 d-flex align-items-center ">
          <p><?=$more_btn ?></p> <i class="fas fa-chevron-circle-right"></i>
        </a>
      </div>
      <?php endwhile; wp_reset_postdata(); ?>
    </div>
    <div class="tab-pane fade" id="pills-director" role="tabpanel" aria-labelledby="pills-director-tab">
      <div class="d-flex justify-content-center">
        <div class="col-12 col-md-8 text-center">
          <?php $query = new WP_Query( 'p=699');
                           while($query->have_posts()) : $query->the_post();?>
          <div class="w-100">
            <img class="photo mb-3 p-3" src="<?= get_the_post_thumbnail_url( $post_id, 'medium' );?>" alt="">
          </div>
          <b class="f20">
            <?php the_title(); ?></b>
          <p class="f16 my-3">
            <?php the_content(); ?>
          </p>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="pills-board" role="tabpanel" aria-labelledby="pills-board-tab">
      <div class="row w-sm-scroll">
        <?php $cat_sovet=5;
                  $cat_care=6;
                  $cat_sci=7;
                  $cat_acts=8;
                   $query = new WP_Query( 'posts_per_page=-1&cat='.$cat_sovet);
                           while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
        <div class="col-11 col-md-6 mb-5">
          <div class="row justify-content-md-left justify-content-center ">
            <div class="col-md-3 col-5 p-0 d-top"><img class="photo p-2" src="<?= get_the_post_thumbnail_url( $post_id,'tab-thumb');?>" alt=""></div>
            <div class="col-md-9 col-11 about_tabtext">
              <b class="f20">
                <?php the_title(); ?></b>
              <div class="f16 my-3">
                <?php the_content();?>
              </div>
            </div>
          </div>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
    <div class="tab-pane fade" id="pills-care" role="tabpanel" aria-labelledby="pills-care-tab">
      <div class="row w-sm-scroll">
        <?php $query = new WP_Query( 'posts_per_page=-1&cat='.$cat_care);
                       while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
        <div class="col-11 col-md-6 mb-5">
          <div class="row justify-content-md-left justify-content-center ">
            <div class="col-md-3 col-5 p-0 d-top"><img class="photo p-2" src="<?= get_the_post_thumbnail_url( $post_id,'tab-thumb');?>" alt=""></div>
            <div class="col-md-9 col-11 about_tabtext">
              <b class="f20">
                <?php the_title(); ?></b>
              <didiv class="f16 my-3">
                <?php the_content();?>
              </didiv>
            </div>
          </div>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
    <div class="tab-pane fade" id="pills-science" role="tabpanel" aria-labelledby="pills-science-tab">
      <div class="row w-sm-scroll">
        <?php $query = new WP_Query( 'posts_per_page=-1&cat='.$cat_sci);
                       while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
        <div class="col-11 col-md-6 mb-5">
          <div class="row justify-content-md-left justify-content-center ">
            <div class="col-md-3 col-5 p-0 d-top"><img class="photo p-2" src="<?= get_the_post_thumbnail_url( $post_id,'tab-thumb');?>" alt=""></div>
            <div class="col-md-9 col-11 about_tabtext">
              <b class="f20">
                <?php the_title(); ?></b>
              <div class="f16 my-3">
                <?php the_content();?>
              </div>
            </div>
          </div>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
    <div class="tab-pane fade" id="pills-acts" role="tabpanel" aria-labelledby="pills-acts-tab">
      <div class="row">
        <?php $query = new WP_Query( 'posts_per_page=-1&cat='.$cat_acts);
                       while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
        <div class="col-md-6 col-12">
          <p class="f20"> <a href="<?= get_permalink(); ?>"><?php the_title(); ?></a></p>
          <p class="f16"> <?php trim_excerpt_chars(200, '...'); ?></p>
        </div>
        <?php endwhile; wp_reset_postdata(); ?>
      </div>
    </div>
  </div>
</div>
