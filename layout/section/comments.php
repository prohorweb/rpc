<section class="mt-3">

  <!-- Далее содержимое вкладок: -->
  <!-- Содержимое первой вкладки "комментарии к статье" -->
  <div class="row">

    <!-- эта строка скажет что их нет  -->

    <!-- форма ввода комментария -->

<div class="col-12">
    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
      <?php if (!$user_ID) : // выводим три поля для гостей?>
      <?php endif; // Конец вывода трех полей для гостей?>
      <div>
        <!-- блок для формы ввода текста комментария  -->
        <p><textarea name="comment" rows="5" id="comment" class="comment-area w-100"></textarea></p> <!-- ширина 83 символа  -->
        <p><input name="submit" type="submit" value="Отправить" class="button float-right px-3 f14" /> <!-- кнопка отправить  -->
          <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" /></p>
      </div>
    </form> <!-- конец формы  -->
</div>

    <ol id="thecomments">

      <?php foreach ($comments as $comment) : ?>
      <?php $comment_type = get_comment_type(); ?>
      <!-- проверка типа комментария -->
      <?php if ($comment_type == 'comment') { ?>
      <!-- выводим только коммены, без пингов и бэков -->

      <?php if ($comment->comment_approved == '0') : ?>
      <!-- проверка на одобрение админом  -->
      <span class="comment-i">Ваш комментарий ожидает модерации. </span>
      <?php endif; ?>
      <!-- конец вывода текста о проверки на одобрение админом  -->
      <div class="comment-text bg-grey-light p-4 my-3 w-100">
        <?php comment_text(); ?>
        <!-- блок с текстом комментария  -->
      </div>
      <div class="clear"></div> <!-- очистка позиционирования  -->
      </li>
      <?php } ?>
      <?php endforeach; ?>
    </ol> <!-- конец нумерованного списка список -->

  </div>



</section><!-- конец блока страницы комментариев -->
