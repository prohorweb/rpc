<?php
if (is_main_site()) {
    $all_title='Все';
} else {
    $all_title='Wszystko';
}
 ?>
 <ul class="nav menu-tabs w-lg-scroll f16" id="eventsTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="true"><?= $all_title ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="two" aria-selected="false"><?=get_cat_name(19) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="three" aria-selected="false"><?=get_cat_name(20) ?></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="four-tab" data-toggle="tab" href="#four" role="tab" aria-controls="four" aria-selected="false"><?=get_cat_name(21) ?></a>
  </li>
</ul>

<div class="tab-content" id="eventsTabContent">
  <div class="tab-pane fade show active" id="one" role="tabpanel" aria-labelledby="one-tab">
    <div class="row col-mobile_event w-md-scroll">
      <?php $query = new WP_Query( 'posts_per_page=4&cat=18');
      if ( $query->have_posts() ) {
                   while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="one-event col-lg-3 col-md-4 col-6">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; ?>
      <div class="d-flex justify-content-center w-100 ">
        <div class="more-events-btn button f14 d-flex align-items-center ">
          <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
        </div>
      </div>
      <?php } else {
 no_found($text);
     }wp_reset_postdata(); ?>

      <?php  $query = new WP_Query( 'posts_per_page=8&offset=4&cat=18');
              if ( $query->have_posts() ) {
                  while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="col-lg-3 col-md-4 col-6 events-all">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; } wp_reset_postdata();
        $cat_count=get_category(18)->category_count;
       if ($cat_count > 12){?>
      <div class=" d-flex justify-content-center w-100">
        <div class="all-events-btn ">
          <a href="<?= get_category_link(18); ?>" class="button f14 d-flex align-items-center">
            <p><?php btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="two-tab">
    <div class="row col-mobile_event w-lg-scroll">
      <?php $query = new WP_Query( 'posts_per_page=4&cat=19');
      if ( $query->have_posts() ) {
                   while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="one-event col-lg-3 col-md-4 col-6">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; ?>
      <div class="d-flex justify-content-center w-100 ">
        <div class="more-events-btn button f14 d-flex align-items-center ">

          <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
        </div>
      </div>
      <?php } else {
 no_found($text);
     }wp_reset_postdata(); ?>

      <?php  $query = new WP_Query( 'posts_per_page=8&offset=4&cat=19');
              if ( $query->have_posts() ) {
                  while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="col-lg-3 col-md-4 col-6 events-all">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>

      <?php endwhile; } wp_reset_postdata();
        $cat_count=get_category(19)->category_count;
       if ($cat_count > 12){?>
      <div class=" d-flex justify-content-center w-100">
        <div class="all-events-btn ">
          <a href="<?= get_category_link(19); ?>" class="button f14 d-flex align-items-center">
            <p><?php btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="tab-pane fade" id="three" role="tabpanel" aria-labelledby="three-tab">
    <div class="row col-mobile_event w-lg-scroll">
      <?php $query = new WP_Query( 'posts_per_page=4&cat=20');
      if ( $query->have_posts() ) {
                   while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="one-event col-lg-3 col-md-4 col-6">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; ?>
      <div class="d-flex justify-content-center w-100 ">
        <div class="more-events-btn button f14 d-flex align-items-center ">

          <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
        </div>
      </div>
      <?php } else {
 no_found($text);
     }wp_reset_postdata(); ?>

      <?php  $query = new WP_Query( 'posts_per_page=8&offset=4&cat=20');
              if ( $query->have_posts() ) {
                  while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="col-lg-3 col-md-4 col-6 events-all">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; } wp_reset_postdata();
        $cat_count=get_category(20)->category_count;
       if ($cat_count > 12){?>
      <div class=" d-flex justify-content-center w-100">
        <div class="all-events-btn ">
          <a href="<?= get_category_link(20); ?>" class="button f14 d-flex align-items-center">
            <p><?php btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
  <div class="tab-pane fade" id="four" role="tabpanel" aria-labelledby="four-tab">
    <div class="row col-mobile_event w-lg-scroll">
      <?php $query = new WP_Query( 'posts_per_page=4&cat=21');
      if ( $query->have_posts() ) {
                   while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="one-event col-lg-3 col-md-4 col-6">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; ?>
      <div class="d-flex justify-content-center w-100 ">
        <div class="more-events-btn button f14 d-flex align-items-center ">

          <p><?php btn($text); ?></p> <i class="fas fa-chevron-circle-right"></i>
        </div>
      </div>
      <?php } else {
 no_found($text);
     }wp_reset_postdata(); ?>

      <?php  $query = new WP_Query( 'posts_per_page=8&offset=4&cat=21');
              if ( $query->have_posts() ) {
                  while($query->have_posts()) : $query->the_post();   $post_id = get_the_ID();?>
      <div class="col-lg-3 col-md-4 col-6 events-all">
        <a href="<?= get_permalink($post_id); ?>">
          <img class="w-100" src="<?= get_the_post_thumbnail_url( $post_id, 'thumbnail' );?>" alt="">
        </a>
        <a href="<?= get_permalink($post_id); ?>">
          <div class="f16 text-center my-3 title">
            <?php trim_title_chars(50, '...'); ?>
          </div>
        </a>
      </div>
      <?php endwhile; } wp_reset_postdata();
        $cat_count=get_category(21)->category_count;
       if ($cat_count > 12){?>
      <div class=" d-flex justify-content-center w-100">
        <div class="all-events-btn ">
          <a href="<?= get_category_link(21); ?>" class="button f14 d-flex align-items-center">
            <p><?php btn_all($text) ?></p> <i class="fas fa-chevron-circle-right"></i>
          </a>
        </div>
      </div>
      <?php } ?>
    </div>
  </div>
</div>
