<?php
if (is_main_site()) {
    $grant_title='Получить грант';
    $org='Полное наименование организации Соискателя';
    $org_2='Наименование партн. орг. с польской стороны';
    $fio='Ф.И.О.';
    $phone='Телефон';
    $sum_1='Запрашиваемая сумма ₽';
    $sum_2='Сумма софинансирования ₽';
    $theme_title='Тематические направления:';
    $theme_1='Наука и образование';
    $theme_2='Культура и искусство';
    $theme_3='Литература и Издательская деятельность';
    $theme_4='Молодежные и спортивные обмены';
    $theme_5='Другое';
    $desc='Краткое описание сути проекта';
    $grant_btn='Оставить заявку';
    $more_btn='Подробнее';
} else {
    $grant_title='Uzyskaj dotację';
    $org = 'Pełna nazwa organizacji Wnioskodawcy';
    $org_2 = 'Nazwa partnerów. org. ze strony polskiej';
    $fio = 'Pełna nazwa';
     $phone = 'Telefon';
     $sum_1 = 'Żądana kwota ₽';
     $sum_2 = 'Kwota dofinansowania $';
     $theme_title = 'Kierunki tematyczne:';
     $theme_1 = "Nauka i edukacja";
     $theme_2 = 'Kultura i sztuka';
     $theme_3 = "Literatura i publikowanie";
     $theme_4 = "Wymiana młodzieży i sportu";
     $theme_5 = "Inne";
     $desc = 'Krótki opis istoty projektu';
     $grant_btn = 'Zostaw prośbę';
     $more_btn = 'Szczegóły';
}
 ?>

 <div class="row">
  <div class="col-12 col-md-5 col-lg">
    <h1 class="articles_title f46  mb-md-5 mb-3"><?php grants_title($title); ?></h1>
    <div class="text-media p-0 pr-md-4 pr-lg-5 mr-lg-5 pb-lg-5">
      <?php grants_content($content); ?>
    </div>
    <div class="d-flex justify-content-sm-start justify-content-center w-100">
     <a href="#" class="button f14 d-flex align-items-center"><p><?= $more_btn ?></p> <i class="fas fa-chevron-circle-right"></i></a>
    </div>
  </div>
  <div class="form-grants col-12 col-md-7 col-lg ">
      <h1 class="articles_title f46 "><?=  $grant_title ?></h1>
      <div role="form" class="wpcf7" id="wpcf7-f51-o1" lang="ru-RU" dir="ltr">
      <div class="screen-reader-response"></div>
      <form action="/#wpcf7-f50-o1" method="post" class="wpcf7-form" novalidate="novalidate">
        <div style="display: none;">
        <input type="hidden" name="_wpcf7" value="51">
        <input type="hidden" name="_wpcf7_version" value="5.0.5">
        <input type="hidden" name="_wpcf7_locale" value="ru_RU">
        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f51-o1">
        <input type="hidden" name="_wpcf7_container_post" value="0">
        </div>
        <div class="form-top-block">
          <span class="wpcf7-form-control-wrap text-org">
            <input type="text" name="text-org" value="" size="40" aria-invalid="false" placeholder="<?= $org ?>">
          </span>
          <span class="wpcf7-form-control-wrap username">
            <input type="text" name="username" value="" size="40" aria-invalid="false" placeholder="<?= $fio ?>">
          </span>
          <span class="wpcf7-form-control-wrap userphone">
            <input type="tel" name="userphone" value="" size="40" aria-invalid="false" placeholder="<?= $phone ?>">
          </span>
          <span class="wpcf7-form-control-wrap useremail">
            <input type="email" name="useremail" value="" size="40" aria-invalid="false" placeholder="Email">
          </span>
          <span class="wpcf7-form-control-wrap text-sum">
            <input type="text" name="text-sum" value="" size="40" aria-invalid="false" placeholder="<?= $sum_1 ?>">
          </span>
          <span class="wpcf7-form-control-wrap text-org-2">
            <input type="text" name="text-org-2" value="" size="40" aria-invalid="false" placeholder="<?= $org_2 ?>">
          </span>
          <span class="wpcf7-form-control-wrap text-sum-2">
            <input type="text" name="text-sum-2" value="" size="40" aria-invalid="false" placeholder="<?= $sum_2 ?>">
          </span>
        </div>
        <div class="form-check p-0">
            <div class="row">
              <div class="col-12 mb-3 f16"><b><?= $theme_title ?></b></div>
            </div>
            <div class="w-xs-scroll"><div class="d-flex justify-content-between">
              <div class="themes">
                <input class="checkbox science" id="checkbox-science" type="checkbox" name="checkbox-723[]" value="<?= $theme_1 ?>">
                <label for="checkbox-science"></label>
                <p class="f10"><?= $theme_1 ?></p>
              </div>
              <div class="themes">
                <input class="checkbox kultura" id="checkbox-kultura" type="checkbox" name="checkbox-723[]" value="<?= $theme_2 ?>">
                <label for="checkbox-kultura"></label>
                <p class="f10"><?= $theme_2 ?></p>
              </div>
              <div class="themes">
                <input class="checkbox literatura" id="checkbox-literatura" type="checkbox" name="checkbox-723[]" value="<?= $theme_3 ?>">
                <label for="checkbox-literatura"></label>
                <p class="f10"><?= $theme_3 ?></p>
              </div>
              <div class="themes">
                <input class="checkbox sport" id="checkbox-sport" type="checkbox" name="checkbox-723[]" value="<?= $theme_4 ?>ы">
                <label for="checkbox-sport"></label>
                <p class="f10"><?= $theme_4 ?></p>
              </div>
              <div class="themes">
                <input class="checkbox any" id="checkbox-any" type="checkbox" name="checkbox-723[]" value="<?= $theme_5 ?>">
                <label for="checkbox-any"></label>
                <p class="f10"><?= $theme_5 ?></p>
              </div>
            </div></div>

        </div>
        <div class=" mb-2 mt-3">
          <span class="wpcf7-form-control-wrap message"><textarea name="message" cols="40" rows="5" class="form-control" aria-invalid="false" placeholder="<?= $desc ?>"></textarea></span>
        </div>
        <button type="submit" value="<?= $grant_btn ?>" class="button form f14 d-flex align-items-center my-3"><p><?= $grant_btn ?></p> <i class="fas fa-chevron-circle-right"></i></button>

        <div class="wpcf7-response-output wpcf7-display-none"></div>
      </form>
     </div>
    </div>
</div>
