<section id="request-form" class="bg-grey-light">
  <div class="container d-flex justify-content-center">
    <div class="form col-md-11 col-lg-8">
      <h3 class="f20 form-title mb-4  text-center text-md-left">Есть бизнес-задача? Мы ее решим!</h3>
      <div role="form" class="wpcf7" id="wpcf7-f51-o1" lang="ru-RU" dir="ltr">
       <div class="screen-reader-response"></div>
        <form action="/#wpcf7-f51-o1" method="post" class="wpcf7-form" novalidate="novalidate">
          <div style="display: none;">
              <input type="hidden" name="_wpcf7" value="51" />
              <input type="hidden" name="_wpcf7_version" value="5.0.5" />
              <input type="hidden" name="_wpcf7_locale" value="ru_RU" />
              <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f51-o1" />
              <input type="hidden" name="_wpcf7_container_post" value="0" />
          </div>
        <div class="form-check p-0  mb-4">
          <div class="row">
            <div class="form-btn col-lg-6 pb-4 pr-lg-0">
              <input class="checkbox service" id="checkbox-1" type="checkbox" name="checkbox-723[]" value="Мобильное приложение" ><label for="checkbox-1"/></label>
            </div>
            <div class="form-btn col-lg-6 pb-4">
              <input class="checkbox service" id="checkbox-2" type="checkbox" name="checkbox-723[]" value="Веб-сайт" ><label for="checkbox-2"/></label>
            </div>
            <div class="form-btn col-lg-12 pl-ex-lg-0">
              <input class="checkbox service" id="checkbox-3" type="checkbox" name="checkbox-723[]" value="Рекламное сопровождение" ><label for="checkbox-3"/></label>
            </div>
          </div>
        </div>
        <div class="form-check p-0  mb-3 d-none d-sm-block">
          <div class="row pl-3 pr-3">
            <div class="mb-4 mt-2 f16">Выделенный бюджет: <b class="budget-price">до 100 000</b> ₽</div>
            <div class="progress w-100 mb-2">
              <div class="progress-bar" style="width:0%" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                <!-- <div class="chekpoint"></div> -->
              </div>
            </div>
          </div>

          <div class="d-flex justify-content-between mb-2 budget label">
            <div class="col p-0">
              <input class="radio" id="radio-1" name="radio-246" value="до 100 000" checked="checked" type="radio"/>
              <label for="radio-1"></label>
            </div>
            <div class="col p-0">
              <input class="radio" id="radio-2" name="radio-246" value="до 500 000" type="radio"/>
              <label for="radio-2"></label>
            </div>
            <div class="col p-0">
              <input class="radio" id="radio-3" name="radio-246" value="до 1 000 000" required="" type="radio"/>
              <label for="radio-3"></label>
            </div>
            <div class="col p-0">
              <input class="radio" id="radio-4" name="radio-246" value="до 5 000 000" type="radio"/>
              <label for="radio-4"></label>
            </div>
            <div class="col p-0">
              <input class="radio" id="radio-5"  name="radio-246" value="&gt; 10 000 000" type="radio"/>
              <label for="radio-5"></label>
            </div>
          </div>

          <div class="d-flex justify-content-between mb-2 budget">
            <div>
              <p class="price-1">100 000</p>
            </div>
            <div>
              <p class="price">500 000</p>
            </div>
            <div>
              <p class="price">1 000 000</p>
            </div>
            <div>
              <p class="price">5 000 000</p>
            </div>
            <div>
              <p class="price">>10 000 000</p>
            </div>
          </div>
        </div>

        <div class="form-check p-0">
          <div class="row">
            <div class="col-12 mb-3 f16"><b>Платформы</b> (выберите одну или несколько)</div>
          </div>
          <div class="row">
            <div class="col platform">
              <input class="checkbox android" id="checkbox-android" type="checkbox" name="checkbox-724[]" value="Android"/>
              <label for="checkbox-android"></label>
            </div>
            <div class="col platform">
              <input class="checkbox ios" id="checkbox-ios" type="checkbox" name="checkbox-724[]" value="IOS"/>
              <label for="checkbox-ios"></label>
            </div>
            <div class="col platform">
              <input class="checkbox windows" id="checkbox-windows" type="checkbox" name="checkbox-724[]" value="Windows"/>
              <label for="checkbox-windows"></label>
            </div>
            <div class="col platform">
              <input class="checkbox any" id="checkbox-any" type="checkbox" name="checkbox-724[]" value="Другая"/>
              <label for="checkbox-any"></label>
            </div>
            <div class="col platform">
              <input class="checkbox watch" id="checkbox-watch" type="checkbox" name="checkbox-724[]" value="Портативные устройства"/>
              <label for="checkbox-watch"></label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12 mb-2 mt-3">
            <span class="message"><textarea name="message" cols="40" rows="10" class="form-control" aria-invalid="false" placeholder="Опишите задачу"></textarea></span>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 col-sm-6 pr-sm-0 pb-2 pb-sm-0">
            <span class="wpcf7-form-control-wrap username"><input type="text" name="username" value="" size="40" class="form-control wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Ваше имя *" /></span>
          </div>
          <div class="col-md-4 col-sm-6">
            <span class="wpcf7-form-control-wrap useremail"><input type="email" name="useremail" value="" size="40" class="form-control wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Ваш E-mail *" /></span>
          </div>
          <div class="col-md-4 pl-md-0 mt-2 mt-md-0">
            <span class="wpcf7-form-control-wrap userphone"><input type="tel" name="userphone" value="" size="40" class="form-control wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Ваш телефон *" /></span>
         </div>
        </div>
        <div class="row mt-2">
          <div class="col-md-8 f12 p-3">
            Нажимая «Отправить», вы соглашаетесь на обработку своих персональных данных на следующих <a href="<?= get_permalink(44); ?>" target="_blank"><b>условиях</b></a>
          </div>
          <div class="col-md-4 mt-3">
            <button type="submit" value="Отправить сообщение" class=" pl-3 pr-3  align-self-center btn btn-request text-center mt-3" type="button">Оставить заявку <span class="ajax-loader"></span></button>
          </div>
        </div>
        <div class="wpcf7-response-output wpcf7-display-none"></div>
      </form>
       </div>
    </div>
  </div>
</section>
