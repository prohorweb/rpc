
<div class="carousel slide" data-ride="carousel" id="carouselSliderIndicators">
  <div class="carousel-inner">
    <?php $query = new WP_Query( 'pagename=slider&post_type=page' );
                  while($query->have_posts()) : $query->the_post();?>
    <div class="carousel-item active">
      <div class="bg-item" style="background-image: url(<?php the_field('картинка_1'); ?>)">
        <div class="container">
          <div class="d-flex align-items-center justify-content-center slide-content">
            <div class="f-white text-center helvatica">
              <p class="f24 f-sm-18 md-md-5 mb-2"><?php the_field('фонд_1'); ?></p>
              <b class="f56 f-sm-36 f-xs-20" style="line-height: 1.09;letter-spacing: -0.2px;"><?php the_field('заголовок_1'); ?></b>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="bg-item" style="background-image: url(<?php the_field('картинка_2'); ?>)">
        <div class="container">
          <div class="d-flex align-items-center justify-content-center slide-content">
            <div class="f-white text-center">
              <p class="f24 f-sm-18 md-md-5 mb-2"><?php the_field('фонд_2'); ?></p>
              <b class="f56 f-sm-36 f-xs-20" style="line-height: 1.09;letter-spacing: -0.2px;"><?php the_field('заголовок_2'); ?></b>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="carousel-item">
      <div class="bg-item" style="background-image: url(<?php the_field('картинка_3'); ?>)">
        <div class="container">
          <div class="d-flex align-items-center justify-content-center slide-content" >
            <div class="f-white text-center">
              <p class="f24 f-sm-18 md-md-5 mb-2"><?php the_field('фонд_2'); ?></p>
              <b class="f56 f-sm-36 f-xs-20" style="line-height: 1.09;letter-spacing: -0.2px;"><?php the_field('заголовок_3'); ?></b>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endwhile; wp_reset_postdata(); ?>
  </div>
  <!-- <a class="carousel-control-prev" data-slide="prev" href="#carouselSliderIndicators" role="button">
    <i aria-hidden="true" class="fas fa-chevron-left"></i><span class="sr-only">Previous</span></a>
  <a class="carousel-control-next" data-slide="next" href="#carouselSliderIndicators" role="button">
    <i aria-hidden="true" class="fas fa-chevron-right"></i><span class="sr-only">Next</span></a> -->

    <ol class="carousel-indicators">
      <li data-target="#carouselSliderIndicators" data-slide-to="0" class="active"><div class="round"></div></li>
      <li data-target="#carouselSliderIndicators" data-slide-to="1"><div class="round"></div></li>
      <li data-target="#carouselSliderIndicators" data-slide-to="2"><div class="round"></div></li>
    </ol>
</div>
<main class="main-page">

<section id="about-link" class="about">
  <?php get_template_part ('layout/section/about'); ?>
</section>

<section id="articles-link" class="articles">
  <div class="container">
    <h1 class="articles_title f46 m-0"><?=get_cat_name(9) ?></h1>
    <?php get_template_part ('layout/section/articles'); ?>
  </div>
</section>

<section id="events-link" class="events">
  <div class="container">
    <h1 class="articles_title f46 mb-4"><?=get_cat_name(18) ?></h1>
    <?php get_template_part ('layout/section/events'); ?>
  </div>
</section>

<section id="media-link" class="media">
  <div class="container">
    <h1 class="articles_title f46 mb-4"><?=get_cat_name(22) ?></h1>
    <?php get_template_part ('layout/section/media'); ?>
  </div>
</section>

<section id="grants-link" class="grants">
  <div class="container">
    <?php get_template_part ('layout/section/grants'); ?>
  </div>
</section>

<?php get_template_part ('layout/section/partners'); ?>

</main>
