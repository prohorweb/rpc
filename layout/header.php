<!doctype html>
<html lang="ru">

<?php
$post_id =  get_the_ID();
$post_desc = get_post( $post_id);?>
<head>
  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="<?php bloginfo('template_url'); ?>/favicon.ico">

  <meta name="twitter:card" content="summary" />
  <meta property="og:title" content="<?php single_post_title(); ?>" />
  <meta property="og:description" content="<?php $desc_str=strip_tags($post_desc->post_content);  echo substr($desc_str,0,300) . "…";?>" />
  <meta property="og:image" content="<?= the_post_thumbnail_url('thumbnail'); ?>" />

  <title>
    <?= title_header()  ?>
  </title>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/plugins/slick/slick.css" />
  <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/plugins/slick/slick-theme.css" />

  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/fonts/fontawesome-free-5/css/all.css" type="text/css" media="screen" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" />

  <?php wp_head(); ?>
  <!-- Plugins -->
  <link href="<?php bloginfo('template_url'); ?>/assets/plugins/off-canvas/css/component-slidebars.css" rel="stylesheet">

</head>

<body>
  <div canvas="container">
    <header class="header offcanvas-desktop bg-blue">
      <div class="container">
        <nav class="navbar navbar-expand row p-md-0">
          <div class="col-2"><a class="navbar-brand" href="<?php bloginfo('home'); ?>"><img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/img/logo.png"></a></div>
          <div class="navbar-collapse col-7 p-0 d-flex justify-content-around">
            <ul class="col-12 navbar-nav f16 justify-content-around">
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#about-link"><?=get_cat_name(4); ?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#articles-link"><?=get_cat_name(9); ?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#events-link"><?=get_cat_name(18); ?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#media-link"><?=get_cat_name(22); ?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#grants-link"><?php grants_title($title) ?></a>
              </li>
              <li class="nav-item">
                <a class="nav-link smooth p-0" href="<?php bloginfo('home'); ?>/#contacts-link"><?php  contacts_title($title); ?></a>
              </li>
            </ul>
          </div>
          <div class="col d-flex justify-content-around align-items-center ">
            <div class="col-6 d-flex justify-content-around align-items-center  social">
              <a href="https://www.facebook.com/Rospolcentr" target="_blank"><i class="fab fa-facebook-square"></i></a>
              <a href="https://twitter.com/rospolcentr" target="_blank"><i class="fab fa-twitter"></i></a>
              <a href="https://www.instagram.com/rospolcentr/" target="_blank"><i class="fab fa-instagram"></i></a>
            </div>
            <div class="col-4 p-0 d-flex justify-content-center align-items-center lang">
                <a class="pr-1" data-toggle="tooltip"  data-placement="a"  title="Русский"  href="<?= get_site_url(1);?>">
                  <img <?php if ( is_main_site() ) { echo "class='active'" ;} ?> src="<?php bloginfo('template_url');?>/assets/img/russia.svg">
                </a>
                <a class="pl-1" data-toggle="tooltip"  data-placement="a"  title="Polski" href="<?= get_site_url(3);?>">
                  <img <?php if ( !is_main_site() ) { echo "class='active'" ;} ?>src="<?php bloginfo('template_url');?>/assets/img/poland.svg">
                </a>
            </div>
            <div class="col-2 d-flex search-btn">
              <i class="fas fa-search"></i>
            </div>
          </div>
        </nav>
      </div>
    </header>

    <div class="container search-input" style="display:none;">
      <form role="search" method="get" id="searchform" action="<?= home_url( '/' ) ?>" >
        <div class="d-flex">
          <i class="fas fa-search icon-search-text"></i>
          <input class="search-text" placeholder="Поиск по сайту..."  type="text" value="<?php echo get_search_query() ?>" name="s" id="s">
          <i class="fas fa-times search-close"></i>
        </div>
       </form>
    </div>
