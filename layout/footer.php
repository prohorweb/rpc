<?php
if (is_main_site()) {
    $var_3='Разработка и продвижение сайтов';
    $var_4='© 2014 Фонд “Российско-польский центр диалога и согласия"';
    $var_5='Подпишитесь на наши новости';
    $var_6='Будьте в курсе самых выгодных предложений оставьте свою эл. почту';
} else {
   $var_3 = 'Tworzenie i promocja strony internetowej';
   $var_4 = '© Fundacja 2014 "Rosyjsko-Polskie Centrum Dialogu i Zgody"';
   $var_5 = 'Zapisz się do naszego newslettera';
   $var_6 = 'Bądź świadomy najlepszych ofert, zostaw wiadomość e-mail. mail';
}
 ?>

<footer id="contacts-link" class="footer">
  <div class="py-5">
    <div class="container f14">
      <div class="row">
        <div class="col-md-4">
          <img class="w-50 mb-3" src="<?php bloginfo('template_url'); ?>/assets/img/logo.png">
          <?php  $query = new WP_Query('pagename=contacts');
                          while ($query->have_posts()) : $query->the_post(); ?>
          <p>
            <?php the_title() ?>
          </p>
          <?php the_content() ?>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="col-md-4">
          <?php  $query = new WP_Query('name=uslovija');
                          while ($query->have_posts()) : $query->the_post(); ?>
          <p><a href="<?php the_permalink() ?>">
              <?php the_title() ?></a></p>
          <?php endwhile; wp_reset_postdata(); ?>
          <?php  $query = new WP_Query('name=soglashenie');
                          while ($query->have_posts()) : $query->the_post(); ?>
          <p><a href="<?php the_permalink() ?>">
              <?php the_title() ?></a></p>
          <?php endwhile; wp_reset_postdata(); ?>
          <!-- <p><a href="">
              <?= $var_2 ?></a></p> -->

          <p><a href="http://iisystems.biz/" target="_blank">
              <?= $var_3 ?></a></p>
          <p>
            <?= $var_4 ?>
          </p>
        </div>
        <div class="col-md-4 f14">
          <p class="">
            <?= $var_5 ?>
          </p>

          <div class="input-group mb-3">
            <input aria-label="E-mail" class="form-control subscribe f14" placeholder="E-mail" type="text">
            <div class="input-group-append">
              <button class="btn subscribe btn-outline-secondary" type="button">
                <i aria-hidden="true" class="far fa-envelope text-white f24"></i>
              </button>
            </div>
          </div>
          <p>
            <?= $var_6 ?>
          </p>
        </div>
      </div>
    </div>
  </div>
</footer>
</div> <!-- canvas="container" -->


<nav canvas="mobile" class="navbar offcanvas fixed-top navbar-mobile bg-blue px-3 row justify-content-between align-items-center ">
  <button class="navbar-toggler navbar-toggle-offcanvas-left">
    <i class="fas fa-bars"></i>
  </button>

  <a class="navbar-brand mobile m-0 p-0" href="http://rpc.prohorweb.ru"><img class="img-fluid" src="http://rpc.prohorweb.ru/wp-content/themes/rpc/assets/img/logo.png"></a>
  <div class="btn-group">
    <div class="btn-group f12 pr-3">
      <?php if ( is_main_site() ) { ?>
      <div class="dropdown-toggle mobile lang-btn" id="dropdownMenuMobLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?php bloginfo('template_url');?>/assets/img/russia.svg" class="mr-2" style="width:25px;">
      </div>
      <div class="dropdown-menu mobile" aria-labelledby="dropdownMenuMobLink">
        <a class="dropdown-item" href="<?php bloginfo('home'); ?>/pol/"> <img src="<?php bloginfo('template_url');?>/assets/img/poland.svg" class="mr-2" style="width:25px;"></a>
      </div>
      <?php } else {?>
      <div class="dropdown-toggle mobile lang-btn" role="button" id="dropdownMenuMobLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <img src="<?php bloginfo('template_url');?>/assets/img/poland.svg" class="mr-2" style="width:25px;">
      </div>
      <div class="dropdown-menu mobile" aria-labelledby="dropdownMenuMobLink">
        <a class="dropdown-item" href="<?= get_site_url(1);?>"> <img src="<?php bloginfo('template_url');?>/assets/img/russia.svg" class="mr-2" style="width:25px;"></a>
      </div>
      <?php } ?>

    </div>
  </div>
</nav>

<div off-canvas="offcanvas-left left reveal" class="pt-3 bg-blue nav-mobile">
  <ul class="navbar-nav f16">
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0" href="<?php bloginfo('home'); ?>/#about-link">
        <?=get_cat_name(4); ?> <i class="fas fa-angle-right float-right"></i></a>
    </li>
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0" href="<?php bloginfo('home'); ?>/#articles-link">
        <?=get_cat_name(9); ?><i class="fas fa-angle-right float-right"></i></a>
    </li>
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0" href="<?php bloginfo('home'); ?>/#events-link">
        <?=get_cat_name(18); ?> <i class="fas fa-angle-right float-right"></i></a>
    </li>
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0 " href="<?php bloginfo('home'); ?>/#media-link">
        <?=get_cat_name(22); ?> <i class="fas fa-angle-right float-right"></i></a>
    </li>
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0" href="<?php bloginfo('home'); ?>/#grants-link">
        <?php grants_title($title) ?><i class="fas fa-angle-right float-right"></i></a>
    </li>
    <li class="nav-item text-left">
      <a class="nav-link left-link p-0" href="#contacts-link">
        <?php  contacts_title($title); ?> <i class="fas fa-angle-right float-right"></i></a>
    </li>
  </ul>
  <div class="social-mobile mp-5">
    <a href="https://www.facebook.com/Rospolcentr" target="_blank"><i class="fab fa-facebook-square"></i></a>
    <a href="https://twitter.com/rospolcentr" target="_blank"><i class="fab fa-twitter pl-4"></i></a>
    <a href="https://www.instagram.com/rospolcentr/" target="_blank"><i class="fab fa-instagram pl-4"></i></a>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



<script src="<?php bloginfo('template_url'); ?>/assets/plugins/off-canvas/js/slidebars.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/plugins/off-canvas/js/component-slidebars.js"></script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/plugins/slick/slick-min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/style.js" type="text/javascript"></script>


<?php wp_footer(); ?>


</body>

</html>
