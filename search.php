<?php get_template_part('layout/header'); ?>
<div class="container">
<div class="f14 my-4">
    <a href="/">Главная</a>
</div>
  <div class="f14 text-gray-light text-uppercase mb-1">Результаты поиска: </div>
  <h1 class="articles_title f26 mb-2"><?= get_search_query(); ?></h1>
  <div class="col-12 p-0">
    <div class="mb-3">

<!-- <form method="post" action="http://rpc.prohorweb.ru<?= $_SERVER['REQUEST_URI']; ?>">
  <select name="nameselect">
      <option selected="selected" VALUE="ASC">Сначала новые</option>
      <option VALUE="DESC"> Сначала старые </option>
   </select>
   <INPUT TYPE="submit" name="Отправить" />
   <?php $order =$_POST['nameselect'];  ?>
</form>
  -->
  <?php
    $list_s= get_search_query();
    $order = get_query_var('order');
    $cat_order = get_query_var('cat');
  ?>
<!--
<a href="<?php bloginfo('home')?>?s=<?=$list_s?><?php if ($cat_order == true) { echo '&cat='.$cat_order; }?>&order=ASC">Новые</a>
<a href="<?php bloginfo('home')?>?s=<?=$list_s?><?php if ($cat_order == true) { echo '&cat='.$cat_order; }?>&order=DESC">Старые</a> -->



<div class="d-flex">
  <div class="col-10 p-0">
    <span class="search-results-link mr-3">
     <a href="<?php bloginfo('home'); ?>?s=<?= get_search_query(); ?>">Все</a>
    </span>
    <?php if(function_exists('search_filters')) search_filters(); ?>
  </div>
  <div class="col-2 pr-0">
    <div class="dropdown order show">
      <?php if ($order == "ASC") { ?>
      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Сначала новые
      </a> <?php }?>
      <?php if ($order == "DESC") { ?>
      <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Сначала старые
      </a> <?php }?>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
        <a class="dropdown-item" href="<?php bloginfo('home')?>?s=<?=$list_s?><?php if ($cat_order == true) { echo '&cat='.$cat_order; }?>&order=ASC">Сначала новые</a>
        <a class="dropdown-item" href="<?php bloginfo('home')?>?s=<?=$list_s?><?php if ($cat_order == true) { echo '&cat='.$cat_order; }?>&order=DESC">Сначала старые</a>
      </div>
    </div>
  </div>
</div>
    <div class="col-12 p-0">
      <div class="row">
          <?php $list_search = get_search_query();
          $args = array(
            'paged' => $paged,
            'post_type' => 'post',
            'cat' => get_query_var('cat'),
            'category__not_in' => array(4,5,6,7,8,9,28),
            's'=> $list_search,
            'order' => get_query_var('order')
          );
          if ( have_posts() ){
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $temp = $wp_query;
            $wp_query= null;
            $wp_query = new WP_Query($args); // Pagination fix
            while($wp_query->have_posts()) : $wp_query->the_post(); ?>


            <div class="col-3 mb-4">
              <a href="<?= get_permalink(); ?>">
                <img class="w-100" src="<?= get_the_post_thumbnail_url($post_id, 'thumbnail');?>" alt="">
              </a>
              <div class="date">
                <?= get_the_date('d.m.Y');?>
              </div>
              <a href="<?= get_permalink(); ?>">
                <div class="title">
                  <?php trim_title_chars(50, '...'); ?>
                </div>
              </a>
              <a href="<?= get_permalink(); ?>">
                <p class="f16 my-2">
                  <?php trim_excerpt_chars(100, '...'); ?>
                </p>
              </a>
            </div>
        <?php endwhile;  wp_reset_postdata();
        the_posts_pagination( array(
         'end_size' => 2,
         'prev_text'    => __('&#8592; Назад'),
         'next_text'    => __('Далее  &#8594;'),
        ) );      }
        else {?>
        <h4 class="no-result"> Материалов по Вашему запросу нет. Попробуйте изменить запрос.</h4>
        <?php }?>
      </div>
    </div>
  </div>
</div>

<?php get_template_part('layout/footer');?>
