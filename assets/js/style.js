$(document).ready(function() {
  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,

    arrows: false,
    fade: true,
    asNavFor: '.slider-nav',
    autoplay: false
  });
  $('.slider-nav').slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    arrows: false,
    centerMode: true,
    focusOnSelect: true,

    responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $(".search-btn").click(function() {
    $(".search-input").attr({
      "style": "display:block"
    });
  });

  $(".search-close").click(function() {
    $(".search-input").attr({
      "style": "display:none"
    });
  });

  $(".more-btn").click(function() {
    $(".articles-all").attr({
      "style": "display:block"
    });
    $(".all-art-btn").attr({
      "style": "display:block!important"
    });
    $(".more-btn").attr({
      "style": "display:none!important"
    });
  });

  $(".more-events-btn").click(function() {
    $(".events-all").attr({
      "style": "display:block"
    });
    $(".more-events-btn").attr({
      "style": "display:none!important"
    });
    $(".all-events-btn").attr({
      "style": "display:block!important"
    });
  });

  $(".navbar-toggle-offcanvas-left").click(function() {
     $('.fa-bars').toggleClass('active');
  });

  $(".smooth").on("click", function (e) {
    var anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: $(anchor.attr('href')).offset().top
    }, 777);
    e.preventDefault();
    return false;
  });

});
